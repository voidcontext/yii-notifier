module.exports = (grunt) -> 
	grunt.initConfig 
		jshint:
			all: ['yii-notifier/assets/*.js', 'yii-notifier/notificationWidget/assets/ko.notification.js']
		copy:
			demoApp: 
				files: [
					{src: 'yii-notifier/**', dest: '../yii-vNotifier-demo/www/protected/extensions/'}
				]
		watch:
			demoApp:
				files: 'yii-notifier/**'
				tasks: ['copy:demoApp']

	grunt.loadNpmTasks "grunt-contrib-copy"
	grunt.loadNpmTasks "grunt-contrib-watch"
	grunt.loadNpmTasks "grunt-contrib-jshint"

	